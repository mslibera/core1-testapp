<?php

namespace App\Seeders;

use App\CcpsCore\Role;
use Illuminate\Support\Facades\App;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;
use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class WidgetsRolesSeeder extends CcpsValidatedSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        $roles = [
            [
                'name' => 'widgets.viewer',
                'display_name' => 'Widgets - Viewer',
                'description' => 'Can view Widgets',
            ],
            [
                'name' => 'widgets.editor',
                'display_name' => 'Widgets - Editor',
                'description' => 'Can create, edit, and delete Widgets',
            ],
        ];



        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($roles, $this->roleArrayConstruction);
            $this->checkForExistingSeedData($roles, Role::all());

            $mergeData = [
                'source_package' => 'none',
                'created_at' => date("Y-m-d H:i:s", time()),
                'updated_at' => date("Y-m-d H:i:s", time()),
                'editable' => 1
            ];

            $this->commitSeedData($roles, 'ccps_roles', $mergeData);

        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
