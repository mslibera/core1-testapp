<?php

namespace App\Seeders;

use App\CcpsCore\Permission;
use Illuminate\Support\Facades\App;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;
use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class WidgetsPermissionsSeeder extends CcpsValidatedSeeder
{
    public $permissions = [
        [
            'name' => 'widgets.create',
            'display_name' => 'Widgets - Create',
            'description' => 'Create Widgets',
        ],
        [
            'name' => 'widgets.read',
            'display_name' => 'Widgets - Read',
            'description' => 'Read / View Widgets',
        ],
        [
            'name' => 'widgets.update',
            'display_name' => 'Widgets - Update',
            'description' => 'Update / Edit Widgets',
        ],
        [
            'name' => 'widgets.delete',
            'display_name' => 'Widgets - Delete',
            'description' => 'Delete Widgets',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($this->permissions, $this->permissionArrayConstruction);
            $this->checkForExistingSeedData($this->permissions, Permission::all());

            $mergeData = [
                'source_package' => 'none',
                'created_at' => date("Y-m-d H:i:s", time()),
                'updated_at' => date("Y-m-d H:i:s", time()),
                'editable' => 1
            ];

            $this->commitSeedData($this->permissions, 'ccps_permissions', $mergeData);

        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
