<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Widget;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Uncgits\Ccps\Support\CcpsPaginator;

class WidgetsController extends Controller
{
    /**
     * Constructor
     *
     */

    public function __construct() {
        // Basic ACL middleware using Laratrust

        $this->middleware('permission:widgets.create')->only(['create', 'store']);
        $this->middleware('permission:widgets.read')->only(['index', 'show', 'audits']);
        $this->middleware('permission:widgets.update')->only(['edit', 'update']);
        $this->middleware('permission:widgets.delete')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $widgets = Widget::where('name', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->sortable('id')->get();
        } else {
            $widgets = Widget::sortable('id')->get();
        }

        $widgets = new CcpsPaginator($widgets);

        return view('widgets.index', compact('widgets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('widgets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        

        Widget::create($requestData);

        return redirect('widgets')->with('flash_message', 'Widget added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $widget = Widget::findOrFail($id);

        return view('widgets.show', compact('widget'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $widget = Widget::findOrFail($id);

        return view('widgets.edit', compact('widget'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        

        $widget = Widget::findOrFail($id);
        $widget->update($requestData);

        return redirect('widgets')->with('flash_message', 'Widget updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Widget::destroy($id);

        return redirect('widgets')->with('flash_message', 'Widget deleted!');
    }

    public function audits(Widget $widget) {
        $widget->load('audits.user');
        $audits = $widget->audits->sortByDesc('created_at');
        return view('widgets.audits', compact(['widget', 'audits']));
    }
}
