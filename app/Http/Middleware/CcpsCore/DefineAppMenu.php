<?php

namespace App\Http\Middleware\CcpsCore;

use Closure;
use Illuminate\Support\Facades\Auth;
use Lavary\Menu\Facade as Menu;

class DefineAppMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // define application menus here

        Menu::make('nav', function($menu) {
            // base for unauthenticated users
            $menu->add('Home');

            // for logged-in users
            if ($user = Auth::user()) {

                // read modules from config and auto-generate menu
                $adminPrivs = [];
                foreach(config('ccps.modules') as $key => $module) {
                    if ($user->can($module['required_permissions'])) {
                        $adminPrivs[] = $module;
                    }
                }

                if (count($adminPrivs) > 0) {
                    // Define Admin menu
                    $menu->add('Admin', route('admin'));

                    foreach($adminPrivs as $priv => $module) {
                        if ($module['parent'] == 'admin') {
                            $menu->admin->add($module['title'], route($module['index']));
                        } else if (empty($module['parent'])) {
                            $menu->add($module['title'], route($module['index']));
                        }
                    }
                }
            }

        });

        return $next($request);
    }
}
