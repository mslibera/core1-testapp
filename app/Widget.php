<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
    use OwenIt\Auditing\Auditable as AuditableTrait;
    
class Widget extends Model implements AuditableContract
{
    use Sortable;
    use AuditableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'widgets';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public $sortable = ['id', 'created_at', 'updated_at', 'name', 'description'];

    
}
