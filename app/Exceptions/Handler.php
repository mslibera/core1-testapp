<?php

namespace App\Exceptions;

use Exception;
use App\Events\CcpsCore\UncaughtQueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // if we are not in debug mode...
        if (!config('app.debug')) {
            // render model-not-found and method-not-allowed exceptions as 404
            if ($exception instanceof ModelNotFoundException || $exception instanceof MethodNotAllowedHttpException) {
                return $this->loadErrorMiddleware($request, function($request) {
                    return parent::render($request, new NotFoundHttpException());
                });
            } else if (!$this->isHttpException($exception)) {
                // render the exception as 500
                return $this->loadErrorMiddleware($request, function($request) use ($exception) {
                    return response()->view('errors.500', ['exception' => $exception], 500);
                });
            } else {
                // is another flavor of HTTP exception...
                $httpCode = $exception->getStatusCode();
                return $this->loadErrorMiddleware($request, function ($request) use ($exception, $httpCode) {
                    $message = $exception->getMessage() ?: '';
                    return response()->view('errors.' . $httpCode, ['message' => $message, 'exception' => $exception], $httpCode);
                });
            }
        }

        if ($exception instanceof QueryException) {
            event(new UncaughtQueryException($exception));
        }

        // make sure we get session data if we need to output a webpage
        if (is_a($exception, 'Symfony\Component\HttpKernel\Exception\HttpException')) {
            $httpCode = $exception->getStatusCode();
            if ($httpCode != 403) {
                // 403 errors actually do this already, so we don't need to do this with 403s.
                return $this->loadErrorMiddleware($request, function ($request) use ($exception, $httpCode) {
                    $message = $exception->getMessage() ?: '';
                    return response()->view('errors.' . $httpCode, ['message' => $message, 'exception' => $exception], $httpCode);
                });
            }
        }

        // regular ol' exception rendered with regular ol' details (debug only, hopefully)
        return parent::render($request, $exception);
    }

    /**
     * Load the middleware required to show state/session-enabled error pages.
     * @param Request $request
     * @param $callback
     * @return mixed
     */
    protected function loadErrorMiddleware(Request $request, $callback)
    {
        $middleware = (\Route::getMiddlewareGroups()['web_errors']);
        return (new Pipeline($this->container))
            ->send($request)
            ->through($middleware)
            ->then($callback);
    }
}
