<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col control-label">{{ 'Name' }}</label>
    <div class="col">
        <input class="form-control" name="name" type="text" id="name" value="{{ $widget->name or ''}}" >

        {!! $errors->first('name', '<div class="invalid-feedback">:message</div>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="col control-label">{{ 'Description' }}</label>
    <div class="col">
        <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ $widget->description or ''}}</textarea>
        {!! $errors->first('description', '<div class="invalid-feedback">:message</div>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
