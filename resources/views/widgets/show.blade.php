@extends('layouts.wrapper', [
    'pageTitle' => 'Widget | Show'
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Widget {{ $widget->id }}</div>
                    <div class="card-body">

                        <a href="{{ route('widgets.index') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fas fa-arrow-left" aria-hidden="true"></i> Back</button></a>

                        @permission('widgets.update')
                        <a href="{{ route('widgets.edit', ['id' => $widget->id]) }}" title="Edit Widget"><button class="btn btn-primary btn-sm"><i class="fas fa-edit" aria-hidden="true"></i> Edit</button></a>
                        @endpermission

                        @permission('widgets.delete')
                        <form method="POST" action="{{ route('widgets.destroy', ['id' => $widget->id]) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Widget" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fas fa-trash" aria-hidden="true"></i> Delete</button>
                        </form>
                        @endpermission

                        <a href="{{ route('widgets.audits', ['id' => $widget->id]) }}" title="Audits for Widget"><button class="btn btn-light btn-sm"><i class="fas fa-clock" aria-hidden="true"></i> Audits</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $widget->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td>{{ $widget->name }}</td></tr><tr><th> Description </th><td>{{ $widget->description }}</td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
