@extends('layouts.wrapper', [
    'pageTitle' => 'Widget | Index'
])

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header"><h5 class="mb-0">Widgets</h5></div>
                <div class="card-body row">
                    <div class="col">
                        @permission('widgets.create')
                        <a href="{{ route('widgets.create') }}" class="btn btn-success btn-sm" title="Add New Widget">
                            <i class="fas fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        @endpermission
                    </div>

                    <div class="col3">
                        <form method="GET" action="{{ route('widgets.index') }}" accept-charset="UTF-8" role="search">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Search..." aria-label="Search" name="search" value="{{ request('search') }}">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-dark"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>


                    <br/>
                    <br/>
                    <div class="table-responsive">
                        {!! $widgets->header() !!}
                        <table class="table table-striped table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">@sortablelink('id', '#')</th><th scope="col">@sortablelink('name', 'Name')</th><th scope="col">@sortablelink('description', 'Description')</th><th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($widgets as $item)
                                <tr>
                                    <th scope="row">{{ $item->id or $loop->iteration }}</th>
                                    <td>{{ $item->name }}</td><td>{{ $item->description }}</td>
                                    <td>
                                        @permission('widgets.read')
                                        <a href="{{ route('widgets.show',  ['id' => $item->id]) }}" title="View Widget"><button class="btn btn-info btn-sm"><i class="fas fa-eye" aria-hidden="true"></i> View</button></a>
                                        @endpermission

                                        @permission('widgets.update')
                                        <a href="{{ route('widgets.edit',  ['id' => $item->id]) }}" title="Edit Widget"><button class="btn btn-primary btn-sm"><i class="fas fa-edit" aria-hidden="true"></i> Edit</button></a>
                                        @endpermission

                                        @permission('widgets.delete')
                                        <form method="POST" action="{{ route('widgets.destroy', ['id' => $item->id]) }}" accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Widget" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fas fa-trash" aria-hidden="true"></i> Delete</button>
                                        </form>
                                        @endpermission
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row justify-content-center">
                            {!! $widgets->appends(Request::except('page'))->render() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection