@extends('layouts.wrapper', [
    'pageTitle' => 'Widget | Create'
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Create New Widget</div>
                    <div class="card-body">
                        <a href="{{ route('widgets.index') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fas fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ route('widgets.store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('widgets.form')

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
