@extends('layouts.wrapper', [
    'pageTitle' => 'Widget | Audits'
])

@section('content')
    <div class="row">
        <div class="col">
            <h1>Audits for Widget {{ $widget->id }}</h1>
            @include('partials.audit-table', ['audits' => $audits])
        </div>
    </div>
@endsection()